-- tables
-- Table: ISIC
CREATE TABLE ISIC (
    isic_id int NOT NULL PRIMARY KEY,
    isikukood Varchar(255)NOT NULL,
    eesnimi Varchar(255)NOT NULL,
    perekonnanimi Varchar(255)NOT NULL,
    sugu Varchar(255)NOT NULL,
    vanus Varchar(255)NOT NULL,
    sunnipaev date NOT NULL,
    kontakt Varchar(255)NOT NULL
);

-- Table: KARJAMAA
CREATE TABLE KARJAMAA (
    karjamaa_id int NOT NULL PRIMARY KEY,
    pindala Varchar(255)NOT NULL,
    kood Varchar(255)NOT NULL,
    kirjeldus Varchar(255)NOT NULL
);

-- Table: KUULUVUS
CREATE TABLE KUULUVUS (
    kuuluvus_id int NOT NULL PRIMARY KEY,
    loomalaut_id int NOT NULL,
    karjamaa_id int NOT NULL,
    algus date NULL,
    lopp date NULL
);

-- Table: LOOM
CREATE TABLE LOOM (
    loom_id int NOT NULL PRIMARY KEY,
    loom_liik_id int NOT NULL,
    toug_id int NOT NULL,
    kood Varchar(255)NOT NULL,
    huudnimi Varchar(255) NULL,
    sugu Varchar(255)NOT NULL,
    vanus Varchar(255)NOT NULL
);

-- Table: LOOMALAUT
CREATE TABLE LOOMALAUT (
    loomalaut_id int NOT NULL PRIMARY KEY,
    kood Varchar(255)NOT NULL,
    tuup Varchar(255)NOT NULL,
    suurus int NOT NULL,
    algus date NULL,
    lopp date NULL
);

-- Table: LOOM_KARJAMAAL
CREATE TABLE LOOM_KARJAMAAL (
    loom_karjamaal_id int NOT NULL,
    karjamaa_id int NOT NULL,
    loom_id int NOT NULL,
    registrinumber Varchar(255)NOT NULL,
    reeglid Varchar(255)NOT NULL
);

-- Table: LOOM_LIIK
CREATE TABLE LOOM_LIIK (
    loom_liik_id int NOT NULL PRIMARY KEY,
    kood Varchar(255)NOT NULL,
    nimetus Varchar(255)NOT NULL,
    kirjeldus Varchar(255)NOT NULL
);

-- Table: LOOM_RUUMIS
CREATE TABLE LOOM_RUUMIS (
    loom_ruumis_id int NOT NULL PRIMARY KEY,
    loom_id int NOT NULL,
    ruum_id int NOT NULL,
    algus date NULL,
    lopp date NULL
);

-- Table: ROLL
CREATE TABLE ROLL (
    roll_id int NOT NULL PRIMARY KEY,
    nimetus Varchar(255)NOT NULL,
    kirjeldus Varchar(255)NOT NULL
);

-- Table: RUUM
CREATE TABLE RUUM (
    ruum_id int NOT NULL PRIMARY KEY,
    loomalaut_id int NOT NULL,
    kood Varchar(255)NOT NULL,
    suurus Varchar(255)NOT NULL,
    tuup Varchar(255)NOT NULL,
    algus date NULL,
    lopp date NULL
);

-- Table: SEOS
CREATE TABLE SEOS (
    seos_id int NOT NULL PRIMARY KEY,
    loom_id int NOT NULL,
    isic_id int NOT NULL,
    roll_id int NOT NULL,
    algus date NULL,
    lopp date NULL,
    nimetus Varchar(255)NOT NULL,
    kirjeldus Varchar(255)NOT NULL
);

CREATE TABLE TOUG (
    toug_id int NOT NULL PRIMARY KEY,
    loom_liik_id int NOT NULL,
    nimetus Varchar(255)NOT NULL,
    kirjeldus Varchar(255)NOT NULL,
    kood Varchar(255)NOT NULL
);


insert into LOOM_LIIK (kood, nimetus, kirjeldus) values ('10','lehm','kodune piimaloom, emakarjad');
insert into LOOM_LIIK (kood, nimetus, kirjeldus) values ('20','lammas','lokkis juustega m?letsejalt imetaja, emane oinas');
select * from LOOM_LIIK;

insert into TOUG (loom_liik_id, nimetus, kirjeldus, kood) values (1,'Pinzgauer','?likond on valdavalt punane (erinevat tooni), seljal ja k?hul on iseloomulik valge triip','101');
insert into TOUG (loom_liik_id, nimetus, kirjeldus, kood) values (2,'Suffolk','t?u tunnus: must n?gu ja jalad, sarved puuduvad','202');
select * from TOUG;

-- taida tabelid andmetega: tabelisse X pane 6 rida.
insert into LOOM (loom_liik_id, toug_id, kood, huudnimi, sugu, vanus) values (2,2,'001','Jane','n','1');
insert into LOOM (loom_liik_id, toug_id, kood, huudnimi, sugu, vanus) values (1,1,'002','Jack','m','2');
insert into LOOM (loom_liik_id, toug_id, kood, huudnimi, sugu, vanus) values (1,1,'003',NULL,'n','4');
insert into LOOM (loom_liik_id, toug_id, kood, huudnimi, sugu, vanus) values (2,2,'004','Jill','n','3');
insert into LOOM (loom_liik_id, toug_id, kood, huudnimi, sugu, vanus) values (2,2,'005','Justin','m','2');
insert into LOOM (loom_liik_id, toug_id, kood, huudnimi, sugu, vanus) values (1,1,'006','Ju','n','3');
select * from LOOM;



insert into LOOMALAUT (kood, tuup, suurus, algus, lopp) values ('1','middle',200, NULL, NULL);
insert into LOOMALAUT (kood, tuup, suurus, algus, lopp) values ('2','middle',200, NULL, NULL);
insert into LOOMALAUT (kood, tuup, suurus, algus, lopp) values ('3','big',300, NULL, NULL);
insert into LOOMALAUT (kood, tuup, suurus, algus, lopp) values ('4','big',300, NULL, NULL);
select * from LOOMALAUT;



-- taida tabelid andmetega: tabelisse Y pane 7 rida.
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (1,'011','100m2','1',TO_DATE('2016/10/09', 'yyyy/dd/mm'),TO_DATE('2030/10/01', 'yyyy/dd/mm'));
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (2,'021','100m2','2',TO_DATE('2017/10/09', 'yyyy/dd/mm'),TO_DATE('2030/10/01', 'yyyy/dd/mm'));
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (3,'031','100m2','1',NULL,NULL);
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (4,'041','100m2','2',NULL, NULL);
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (1,'012','80m2','1',NULL,NULL);
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (2,'022','80m2','2',TO_DATE('2018/14/10', 'yyyy/dd/mm'),NULL);
insert into RUUM (loomalaut_id, kood, suurus, tuup, algus, lopp) values (3,'032','80m2','1',NULL,NULL);
select * from RUUM;



-- taida tabelid andmetega: tabelisse Z pane 8 rida.
insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (2,6,TO_DATE('2018/14/10', 'yyyy/dd/mm'),NULL);
insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (3,2,TO_DATE('2017/10/09', 'yyyy/dd/mm'),TO_DATE('2030/10/01', 'yyyy/dd/mm'));
insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (6,2,TO_DATE('2017/10/09', 'yyyy/dd/mm'),TO_DATE('2030/10/01', 'yyyy/dd/mm'));

insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (3,6,TO_DATE('2030/10/01', 'yyyy/dd/mm'),NULL);
insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (6,6,TO_DATE('2030/10/01', 'yyyy/dd/mm'),NULL);

insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (1,1,TO_DATE('2016/10/09', 'yyyy/dd/mm'),NULL);
insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (4,1,TO_DATE('2016/10/09', 'yyyy/dd/mm'),NULL);
insert into LOOM_RUUMIS (loom_id, ruum_id, algus, lopp) values (5,1,TO_DATE('2016/10/09', 'yyyy/dd/mm'),TO_DATE('2030/10/01', 'yyyy/dd/mm'));
select * from LOOM_RUUMIS;



-- tehke paring uhte tabelisse.
SELECT loom_liik_id, toug_id, kood, huudnimi, sugu, vanus FROM LOOM ORDER BY huudnimi ASC;

-- tehke paring ule KAHE tabeli.
SELECT * FROM RUUM,LOOM_RUUMIS WHERE RUUM.algus = LOOM_RUUMIS.algus;

-- tehke paring ule KOLME tabeli.
SELECT * FROM LOOM,LOOM_RUUMIS,RUUM WHERE LOOM.loom_id = LOOM_RUUMIS.loom_id and LOOM_RUUMIS.algus = RUUM.algus;
